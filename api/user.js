import fetch from '../common/fetch.js'
import helper from  '../common/helper.js'

const BASE_API = helper.BASE_API
const POST = 'POST'

// 用户获取openId
export function getOpenId(code) {
	let url = BASE_API + '/user/openId'
	let data = {
		code
	}
	
	return fetch(url, POST, data)
}

// 获取用户信息
export function getUserInfo(openId) {
	let url = BASE_API + '/userInfo'
	let data = {
		openId
	}
	
	return fetch(url, POST, data)
}

export function getAdminList() {
	let url = BASE_API + '/admin/list'
	
	return fetch(url, POST)
}

// 添加管理员
export function saveAdmin(phone, password, identity) {
	let url = BASE_API + '/admin/save'
	let data = {
		phone, // string
		password, // string
		identity // int 0超管  1
	}
	
	return fetch(url, POST, data)
}

export function delAdminById(adminId) {
	let url = BASE_API + '/admin/delete'
	let data = {
		adminId
	}
	
	return fetch(url, POST, data)
}

// 管理员登录,即绑定
export function adminLogin(phone, password, photo, nickname, openId) {
	let url = BASE_API + '/admin/login'
	let data = {
		phone, // string
		password,
		photo,
		nickname,
		openId
	}
	
	return fetch(url, POST, data)
}

// 添加用户
export function addUser(passWord, phone, balance) {
	let url = BASE_API + '/user/add'
	let data = {
		passWord, // string
		phone, // string
		balance // int
	}
	
	return fetch(url, POST, data)
}

// 用户登录，即绑定
export function userLogin(phone, password, openId, nickname, photo) {
	let url = BASE_API + '/user/login'
	let data = {
		phone, // string
		password,
		openId,
		nickname,
		photo
	}
	
	return fetch(url, POST, data)
}

// 用户余额充值
export function addBalance(phone, balance) {
	let url = BASE_API + '/user/balance'
	let data = {
		phone,
		balance
	}
	
	return fetch(url, POST, data)
}