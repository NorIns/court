import fetch from '../common/fetch.js'
import helper from  '../common/helper.js'

const BASE_API = helper.BASE_API
const POST = 'POST'

export function addOrder(orderTime, userId, orderVenue, date, scheduleTime, status, phone) {
	let url = BASE_API + '/order/add'
	let data = {
		orderTime, // string 下订单时间
		userId, // int 预定人id
		orderVenue, // int
		date, // string
		scheduleTime, // string
		status, // int  1 已预约 2 已完成 3 已取消
		phone
	}
	
	return fetch(url, POST, data)
}

// 修改订单状态
export function updateOrder(orderId, status) {
	let url = BASE_API + '/order/update'
	let data = {
		orderId, // int
		status // int  1 已预约 2 已完成 3 已取消
	}
	
	return fetch(url, POST, data)
}

// 查看我的订单
export function getMyOrderList(userId) {
	let url = BASE_API + '/order/myList'
	let data = {
		userId // int
	}
	
	return fetch(url, POST, data)
}

// 分状态查看我的订单
export function getMyOrderListByStatus(userId, status) {
	let url = BASE_API + '/order/myStatus'
	let data = {
		userId, // int
		status
	}
	
	return fetch(url, POST, data)
}

// 管理员查看所有订单
export function getOrderList(userId) {
	let url = BASE_API + '/order/list'
	
	return fetch(url, POST)
}

// 管理员分状态查看订单
export function getOrderListByStatus(status) {
	let url = BASE_API + '/order/status'
	let data = {
		status
	}
	
	return fetch(url, POST, data)
}