import fetch from '../common/fetch.js'
import helper from  '../common/helper.js'

const BASE_API = helper.BASE_API
const POST = 'POST'

export function addSite(name, status, picture, price, discount, recommended, ball, rest, floor, place, lamp, loan, wifi) {
	let url = BASE_API + '/site/add'
	let data = {
		name,
		status, // nt  0 停用，1 启用
		picture,
		price, // int
		discount, // int 特惠场馆 0 是 1 不是 ！！！！
		recommended, // int 推荐场馆 0 是 1 不是
		ball, // 球类 0 篮球 1 排球 2 乒乓球 3 网球 4 桌球 5 羽毛球
		rest, // string 休息区
		floor, // string 地板类型
		place, // int 0 室外 1 室内
		lamp, // int 0 无灯光 1 有灯光
		loan, // strng 器材租借
		wifi // int 0 无 1 有
	}
	
	return fetch(url, POST, data)
}

export function deleteSite(siteId) {
	let url = BASE_API + '/site/delete'
	let data = {
		siteId
	}
	
	return fetch(url, POST, data)
}

export function updateSite(siteId, name, status, picture, price, discount, recommended, ball, rest, floor, place, lamp, loan, wifi) {
	let url = BASE_API + '/site/update'
	let data = {
		siteId, // int
		name,
		status, // int  0 停用，1 启用
		picture,
		price, // int
		discount, // int 特惠场馆
		recommended, // int 推荐场馆
		ball, // 球类 0 篮球 1 排球 2 乒乓球 3 网球 4 桌球 5 羽毛球
		rest, // string 休息区
		floor, // string 地板类型
		place, // int 0 室外 1 室内
		lamp, // int
		loan, // strng 器材租借
		wifi // int
	}
	
	return fetch(url, POST, data)
}

// 查看某一场地详细信息
export function getSiteInfo(siteId) {
	let url = BASE_API + '/site/get'
	let data = {
		siteId // int
	}
	
	return fetch(url, POST, data)
}

export function getSiteList(ball) {
	let url = BASE_API + '/site/list'
	let data = {
		ball
	}
	
	return fetch(url, POST, data)
}

// 查看场地已预订时间
export function getSiteSchedule (siteId, date) {
	let url = BASE_API + '/site/schedule'
	let data = {
		siteId,
		date
	}
		
	return fetch(url, POST, data)
}

// 获取特惠场馆列表
export function getDiscountSite() {
	let url = BASE_API + '/site/discount'
	
	return fetch(url, POST)
}

// 获取推荐场馆列表
export function getRecommendSite() {
	let url = BASE_API + '/site/recommended'
	
	return fetch(url, POST)
}

export function qiniuUpload() {
	let url = BASE_API + '/QiniuUpToken'
	
	return fetch(url, POST)
}