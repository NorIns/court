import fetch from '../common/fetch.js';
import helper from '../common/helper';

const BASE_API = helper.BASE_API;
// 发布论坛
export function addForum(content, questionerId, time, picture, title, label) {
  let url = BASE_API + '/question/add';
  let method = 'POST';
  let data = {
    content,
    questionerId,
    time,
    picture,
    title,
    label
  };

  return fetch(url, method, data);
}

// 获取论坛列表
export function getForumList() {
  let url = BASE_API + '/question/list';
  let method = 'POST';

  return fetch(url, method);
}

// 获取我的论坛列表
export function getMyForumList(userId) {
  let url = BASE_API + '/question/mylist';
  let method = 'POST';
  let data = {
    userId
  };

  return fetch(url, method, data);
}

// 获取论坛详情
export function getForumDetail(questionId, userId) {
  let url = BASE_API + '/question/get';
  let method = 'POST';
  let data = {
    questionId
  };

  return fetch(url, method, data);
}

// 删除论坛
export function deleteMyForum(questionId, userId) {
  let url = BASE_API + '/question/delete';
  let method = 'POST';
  let data = {
    questionId,
    userId
  };

  return fetch(url, method, data);
}

// 回复论坛
export function replyForum(content, answerManId, time, questionId) {
  let url = BASE_API + '/answer/add';
  let method = 'POST';
  let data = {
    content,
    answerManId,
    time,
    questionId
  };

  return fetch(url, method, data);
}

// 删除论坛的回复
export function deleteMyReply(answerId, answerManId) {
  let url = BASE_API + '/answer/delete';
  let method = 'POST';
  let data = {
    answerId,
    answerManId
  };

  return fetch(url, method, data);
}

// 点赞论坛的回复
export function thumbsupReplay(userId, itemKeyId, status) {
  let url = BASE_API + '/thumbsup';
  let method = 'POST';
  let data = {
    userId,
    itemKeyId,
    status
  };

  return fetch(url, method, data);
}

// 浏览量
export function addScan(userId, itemKeyId) {
  let url = BASE_API + '/scan/add';
  let method = 'POST';
  let data = {
    userId,
    itemKeyId
  };

  return fetch(url, method, data);
}
