export default function fetch(url, method, data) {
  return new Promise((reslove, reject) => {
    uni.request({
      url,
      method,
      data,
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: res => {
				if(res.statusCode === 200 || res.statusCode === 304) {
					return reslove(res);
				} else {
					return reject(res)
				}
      },
      fail: err => {
        return reject(err);
      }
    });
  });
}
