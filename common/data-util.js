export function getNowTime() {
  const now = new Date();
  const nowArr = now.toString().split(' ');
  const month = now.getMonth() + 1;
  const time = `${nowArr[3]}-${month}-${nowArr[2]} ${nowArr[4]}`;
  return time;
}
